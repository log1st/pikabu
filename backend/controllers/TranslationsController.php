<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 14:11
 */

namespace backend\controllers;


use backend\components\BaseCRUDController;
use backend\models\I18nTranslationSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class TranslationsController extends BaseCRUDController
{

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [ 'admin' ]
                    ]
                ]
            ]
        ]);
    }

    public function modelClass()
    {
        return I18nTranslationSearch::className();
    }
}