<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 20:51
 */

namespace backend\controllers;


use backend\models\forms\SignInForm;
use common\components\BaseController;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class AuthController extends BaseController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    'heh' => [
                        'allow' => true,
                        'actions' => [ 'sign-in', 'sign-up' ],
                        'roles' => [ '?' ],
                    ],
                    [
                        'allow' => true,
                        'actions' => [ 'sign-out' ],
                        'roles' => [ '@' ],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    if(in_array($action->id, [ 'sign-in', 'sign-up' ])) {
                        return $this->redirect(['site/index']);
                    }
                    return false;
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'sign-out' => [ 'post' ]
                ]
            ]
        ]);
    }

    public function actionIndex() {
        return $this->redirect([ 'auth/sign-in' ]);
    }

    public function actionSignIn() {
        $model = new SignInForm();

        $r = Yii::$app->request;

        if($model->load($r->post())) {
            if($model->validate()) {
                $model->login();
                return $this->redirect($model->getRedirectUrl());
            }
        }

        return $this->render('sign-in', [
            'model' => $model
        ]);
    }

    public function actionSignOut() {
        Yii::$app->user->logout();
        return $this->redirect(['site/index']);
    }
}