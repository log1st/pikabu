<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$rules = [
    'i18n/<controller:(sources|translations)>' => '<controller>/index',
    'i18n/<controller:(sources|translations)>/<id:\d+>' => '<controller>/view',
    'i18n/<controller:(sources|translations)>/<id:\d+>/<action:([\w\-])+>' => '<controller>/<action>',
    'i18n/<controller:(sources|translations)>/<action:([\w\-])+>' => '<controller>/<action>',
];

return [
    'id' => 'backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'components' => [
        'urlManager' => [
            'rules' => $rules
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'cookieValidationKey' => '_cookie-backend'
        ],
        'user' => [
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-backend',
                'httpOnly' => true
            ],
        ],
        'session' => [
            'name' => 'advanced-backend',
        ],
    ],
    'params' => $params,
];
