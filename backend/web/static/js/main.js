function initTooltips(parent) {
    var tooltips = $(parent).find('[data-rel="tooltip"]');
    tooltips.each(function(i, tooltip) {
        tooltip = $(tooltip);

        tooltip.tooltip();
    })
}

function initTabs() {
    var groups = $('[data-rel="tabs"]');

    groups.each(function(i, group) {
        group = $(group);

        var headers = group.find('[data-rel="tabs-header"]'),
            panes = group.find('[data-rel="tabs-pane"]');

        headers.each(function(i, header) {
            header = $(header);

            header.on('click', function() {
                var pane = panes.filter(header.attr('href'));

                headers.removeClass('active');
                panes.removeClass('active');

                header.addClass('active');
                pane.addClass('active');

                return false;
            });
        })
    })
}

function reInit(parent) {
    if(typeof parent === 'undefined') {
        parent = $('body');
    }
    initTooltips(parent);
    initTabs(parent);
}

$(function() {
    reInit();
});