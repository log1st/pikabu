<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:33
 */

namespace backend\components;


use backend\models\forms\SearchForm;
use backend\traits\SearchModelTrait;
use backend\widgets\ListView;
use backend\widgets\Record;
use common\components\BaseActiveRecord;
use common\components\BaseController;
use common\components\Pagination;
use common\components\Sort;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

abstract class BaseCRUDController extends BaseController
{
    abstract public function modelClass();

    /** @var BaseActiveRecord|SearchModelTrait */
    public static $instance;

    public function init() {
        $class = $this->modelClass();
        if(!class_exists($class)) {
            throw new ServerErrorHttpException(Yii::t(null, 'unknownModelClass'));
        }
        static::$instance = new $class([
            'scenario' => 'search'
        ]);

        parent::init();
    }

    private function buildQuery() {
        $this->_query = (clone static::$instance)::find();
    }

    /**
     * @param $params
     * @return SearchModelTrait|BaseActiveRecord
     */
    private function getInstance($params = []) {

        $instance = self::$instance;

        $instance->load($params);
        $instance->validate();

        return $instance;
    }

    /** @var ActiveQuery */
    public $_query;
    private function filterQuery($params) {
        $instance = $this->getInstance($params);

        $config = $instance::searchConfig();

        foreach($instance->attributes as $attribute => $value) {
            $param = "{$instance::tableName()}.$attribute";
            if(!empty($value)) {
                if (in_array($attribute, $config['equal'] ?? [])) {
                    $this->_query->andWhere([
                        $param => $value
                    ]);
                }   else {
                    $this->_query->andWhere([
                        'like', $param, $value
                    ]);
                }
            }
        }

        return $instance;
    }

    private function buildParameters() {

    }

    /** @var ActiveDataProvider */
    public $_provider;
    private $limit = 10;
    private function buildProvider() {
        $instance = self::$instance;

        $r = Yii::$app->request;

        $search = new SearchForm([
            'scenario' => 'search'
        ]);
        $data = [
            'SearchForm' => $r->get()
        ];
        $search->load($data);
        $search->validate();

        $this->_query->groupBy($instance::tableName() . '.' . $instance::$idStr);

        $this->_provider = new ActiveDataProvider([
            'query' => $this->_query,
            'pagination' => [
                'class' => Pagination::className(),
                'search' => $search,
                'pageSize' => $search->limit
            ],
            'sort' => [
                'class' => Sort::className(),
                'defaultOrder' => [
                    $instance::$idStr => SORT_ASC
                ]
            ]
        ]);

        return $search;
    }

    final public function getRecord($id) {
        $this->buildQuery();

        $instance = self::$instance;

        $this->filterQuery([
            $instance->formName() => [
                $instance::$idStr => $id
            ]
        ]);

        $record = $this->_query->one();

        if(!$record) {
            throw new HttpException(404, Yii::t(null, 'recordNotFound'));
        }

        return $record;
    }

    final public function actionView($id) {
        $record = $this->getRecord($id);

        return $this->renderContent(
            Record::widget([
                'scenario' => 'view',
                'record' => $record
            ])
        );
    }

    final public function actionIndex() {
        $r = Yii::$app->request;

        $this->buildQuery();

        $instance = $this->filterQuery($r->get());

        $this->buildParameters();

        $search = $this->buildProvider();

        return $this->renderContent(
            ListView::widget([
                'provider' => $this->_provider,
                'config' => (self::$instance)::config(),
                'actions' => (self::$instance)::actions(),
                'instance' => $instance,
                'search' => $search
            ])
        );
    }
}