<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 25.03.2017
 * Time: 0:53
 */

namespace backend\helpers;


use backend\traits\SearchModelTrait;
use common\components\BaseActiveRecord;

class SearchModelView
{
    public static function getAttributeParam($config, $key) {
        if(is_string($config)) {
            return $config;
        }   else {
            $attr = "attribute_$key";
            return is_array($config) ? ($config['attribute'] ?? $attr) : $attr;
        }
    }

    public static function actions() {
        return [
            'view' => [
                'id' => 'view',
                'icon' => 'fa fa-eye',
                'btnClass' => 'success',
                'url' => function($record) {
                    /** @var $record SearchModelTrait */
                    return $record->getCrudLink();
                },
            ],
            'update' => [
                'id' => 'update',
                'icon' => 'fa fa-pencil',
                'btnClass' => 'primary',
                'visibility' => function($record) {
                    /** @var $record BaseActiveRecord */
                    return in_array('update', $record->scenarios());
                },
                'url' => function($record) {
                    /** @var $record SearchModelTrait */
                    return $record->getCrudLink('update');
                },
            ],
            'delete' => [
                'id' => 'delete',
                'icon' => 'fa fa-trash',
                'btnClass' => 'danger',
                'visibility' => function($record) {
                    /** @var $record BaseActiveRecord */
                    return in_array('delete', $record->scenarios());
                },
                'url' => function($record) {
                    /** @var $record SearchModelTrait */
                    return $record->getCrudLink('delete');
                },
            ],
        ];
    }
}