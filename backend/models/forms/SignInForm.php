<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 6:23
 */

namespace backend\models\forms;


use common\components\Model;
use common\models\User;
use Yii;

class SignInForm extends Model
{
    public $login;
    public $password;
    public $rememberMe;

    private $_user;

    public function rules() {
        return [
            [ [ 'login', 'password', 'rememberMe' ], 'required' ],
            [ [ 'rememberMe' ], 'boolean' ],
            [ [ 'password' ], 'validatePassword' ]
        ];
    }

    public function validatePassword() {
        if(!$this->hasErrors()) {
            $user = $this->getUser();

            if(!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', Yii::t(null, 'rule.validatePassword.error'));
                return false;
            }
        }
        return true;
    }

    /**
     * @return User
     */
    public function getUser() {
        if(!$this->_user) {
            $this->_user = User::findByIdentity($this->login);
        }

        return $this->_user;
    }

    public function login() {
        Yii::$app->user->login($this->getUser(), $this->rememberMe ? Yii::$app->params['rememberMe'] : 0);
    }

    public function getRedirectUrl() {
        return [ 'site/index' ];
    }

    public function attributeLabels()
    {
        $return = [];
        foreach($this->attributes as $key => $attribute) {
            $return[$key] = Yii::t(null, 'attributeLabel.' . $key);
        }
        return $return;
    }
}