<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 31.03.2017
 * Time: 5:42
 */

namespace backend\models\forms;


use common\components\Model;

class SearchForm extends Model
{
    public $limit = 10;
    public $sort;
    public $page = 1;

    public static function limits() {
        return [ 5, 10, 20, 50, 100 ];
    }

    public function scenarios()
    {
        return [
            'search' => [ 'limit', 'sort', 'page' ]
        ];
    }

    public function rules() {
        return [
            [ 'limit', 'in', 'range' => self::limits() ],
            [ 'page', 'integer', 'min' => 1 ]
        ];
    }

    public function afterValidate()
    {
        parent::afterValidate();

        $simple = new self;

        foreach($this->attributes as $attribute => $key) {
            if($this->hasErrors($attribute)) {
                $this->{$attribute} = $simple->{$attribute};
            }
        }
    }
}