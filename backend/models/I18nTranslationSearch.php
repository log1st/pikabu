<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:38
 */

namespace backend\models;


use backend\traits\SearchModelTrait;
use common\helpers\Html;
use common\models\I18nSource;
use common\models\I18nTranslation;
use yii\helpers\ArrayHelper;

class I18nTranslationSearch extends I18nTranslation
{
    use SearchModelTrait;

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            'search' => [ self::$idStr, '_source_id', 'language', 'translation' ],
            'insert' => [ '_source_id', 'language', 'translation' ],
            'update' => [ '_source_id', 'language', 'translation' ],
        ]);
    }

    public function rules() {
        return [
            [ [ '_source_id', 'language', 'translation' ], 'required', 'on' => [ 'insert', 'update' ] ],
        ];
    }

    static function searchConfig(): array
    {
        return [
            'equal' => [ self::$idStr ]
        ];
    }

    static function cnc(): string
    {
        return 'translations';
    }

    static function config(): array
    {
        return [
            'main' => [
                'list' => [
                    [
                        'attribute' => self::$idStr,
                        'header' => '#',
                        'options' => [
                            'style' => [
                                'width' => '120px'
                            ]
                        ]
                    ],
                    [
                        'attribute' => '_source_id',
                        'value' =>  [
                            'field' => 'href',
                            'url' => function(I18nTranslation $record) {
                                $source = $record->_source;
                                return $source ? $source->getCrudLink() : Html::na();
                            },
                            'caption' => function($record) {
                                $source = $record->_source;
                                return $source ?? Html::na();
                            }
                        ],
                        'filter' => [
                            'field' => 'dropDown',
                            'values' => I18nSource::findAllAsArray('id', ':category &raquo; :message', ['category','message'])
                        ]
                    ],
                    'language',
                    'translation'
                ],
            ]
        ];
    }

    static function actions(): array
    {
        return [];
    }

    public function get_source() {
        return $this->hasOne(I18nSourceSearch::className(), [
            I18nSource::$idStr => '_' . I18nSource::$idStr
        ]);
    }
}