<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:38
 */

namespace backend\models;


use backend\traits\SearchModelTrait;
use common\components\BaseActiveRecord;
use common\models\I18nSource;
use yii\helpers\ArrayHelper;

class I18nSourceSearch extends I18nSource
{
    use SearchModelTrait;

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            'search' => [ self::$idStr, 'category', 'message' ],
            'insert' => [ 'category', 'message' ],
            'update' => [ 'category', 'message' ],
        ]);
    }

    public function rules() {
        return [
            [ [ 'category', 'message' ], 'required', 'on' => [ 'insert', 'update' ] ],
            [ [ 'category' ], 'unique', 'targetAttribute' => [ 'category', 'message' ] ]
        ];
    }

    static function searchConfig(): array
    {
        return [
            'equal' => [ self::$idStr ]
        ];
    }

    static function cnc(): string
    {
        return 'sources';
    }

    static function config(): array
    {
        return [
            'main' => [
                'list' => [
                    [
                        'attribute' => self::$idStr,
                        'header' => '#',
                        'options' => [
                            'style' => [
                                'width' => '120px'
                            ]
                        ]
                    ],
                    'category',
                    'message'
                ],
            ],
            'translations' => [
                'list' => function($record) {
                    return '';
                }
            ]
        ];
    }

    static function actions(): array
    {
        return [
            [
                'id' => 'translate',
                'icon' => 'fa fa-language',
                'btnClass' => 'secondary',
                'visibility' => function($record) {
                    $translation = new I18nTranslationSearch;
                    return in_array('create', $translation->scenarios());
                },
                'url' => function($source) {
                    $translation = new I18nTranslationSearch;
                    return $translation->getCrudLink('create', [
                        $source::$idStr => $source->id
                    ]);
                }
            ]
        ];
    }
}