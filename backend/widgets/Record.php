<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 08.04.2017
 * Time: 2:00
 */

namespace backend\widgets;


use backend\traits\SearchModelTrait;
use common\components\BaseActiveRecord;
use yii\base\Widget;

class Record extends Widget
{
    public $scenario = 'view';
    /** @var BaseActiveRecord|SearchModelTrait */
    public $record = null;

    public function run() {
        $record = $this->record;

        return $this->render('record/main', [
            'scenario' => $this->scenario,
            'record' => $this->record,
            'config' => $record::config()
        ]);
    }
}