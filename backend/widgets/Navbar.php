<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:53
 */

namespace backend\widgets;


use yii\helpers\ArrayHelper;

class Navbar extends \common\widgets\Navbar\Navbar
{
    public function init() {
        parent::init();

        $this->menu = ArrayHelper::merge($this->menu, [
            'main' => [
                'list' => [
                    [
                        'attribute' => 'i18n',
                        'url' => '#',
                        'children' => [
                            [
                                'attribute' => 'sources',
                                'url' => [ 'sources/index' ]
                            ],
                            [
                                'attribute' => 'translations',
                                'url' => [ 'translations/index' ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }
}