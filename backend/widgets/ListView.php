<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 24.03.2017
 * Time: 23:48
 */

namespace backend\widgets;


use backend\models\forms\SearchForm;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

class ListView extends Widget
{
    /** @var  ActiveDataProvider */
    public $provider;
    /** @var  array */
    public $config;
    /** @var  array */
    public $actions;

    public $instance = null;

    /** @var SearchForm */
    public $search = null;

    public function init() {
        parent::init();
    }

    public function run() {
        return $this->render('listView/index', [
            'provider' => $this->provider,
            'config' => $this->config,
            'actions' => $this->actions,
            'instance' => $this->instance,
            'search' => $this->search
        ]);
    }
}