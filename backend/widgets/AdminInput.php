<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 31.03.2017
 * Time: 1:56
 */

namespace backend\widgets;


use yii\base\Widget;

class AdminInput extends Widget
{
    public $field = 'input';
    public $record = null;
    public $attribute = null;
    public $options = [];
    public $form = null;

    public function run() {
        return $this->render('adminInput/main', [
            'field' => $this->field,
            'record' => $this->record,
            'attribute' => $this->attribute,
            'options' => $this->options,
            'form' => $this->form
        ]);
    }
}