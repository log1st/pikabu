<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 08.04.2017
 * Time: 0:44
 * @var $config
 * @var $record
 */
use common\helpers\Html;

?>

<?php if(count((array)$config)):?>
    <div class="btn-group">
        <?php foreach($config as $key => $button):?>
            <?php if($button instanceof Closure):?>
                <?=call_user_func($button, $record)?>
            <?php else:
                $visibility = true;
                if(isset($button['visibility'])) {
                    if($button['visibility'] instanceof  Closure) {
                        $visibility = call_user_func($button['visibility'], $record);
                    }   else {
                        $visibility = (boolean)$button['visibility'];
                    }
                }
                $url = '#';
                if(isset($button['url'])) {
                    if($button['url'] instanceof Closure) {
                        $url = call_user_func($button['url'], $record);
                    }   else {
                        $url = $button['url'];
                    }
                }
                $caption = $button['caption'] ?? Yii::t(null, 'action.' . ($button['id'] ?? $key));
                echo Html::a(Html::icon($button['icon'] ?? 'fa fa-dot'), $url, \yii\helpers\ArrayHelper::merge([
                    'class' => 'btn btn-square btn-sm btn-' . ($button['btnClass'] ?? 'secondary'),
                    'data-rel' => 'tooltip',
                    'title' => $caption
                ], $button['options'] ?? []));
            endif; ?>
        <?php endforeach?>
    </div>
<?php endif?>
