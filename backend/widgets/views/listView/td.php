<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 25.03.2017
 * Time: 0:44
 *
 * @var $record \backend\traits\SearchModelTrait|\common\components\BaseActiveRecord
 * @var $config array
 * @var $attribute string
 */

use common\helpers\Html;
use yii\helpers\Url;

$caption = '';

if(is_string($config) || !isset($config['value'])) {
    $caption = $record->{$attribute} ?? Html::na();
}   elseif($config['value'] instanceof Closure) {
    $caption = call_user_func($config['value'], $record);
}   elseif(is_array($config['value'])) {
    $field = $config['value']['field'] ?? 'href';

    if(false) {

    }   else {
        if(!isset($config['value']['url'])) {
            $url = false;
        }   elseif($config['value']['url'] instanceof Closure) {
            $url = call_user_func($config['value']['url'], $record);
        }   elseif(is_array($config['value']['url'])) {
            $url = Url::to($config['value']['url']);
        }   else {
            $url = $config['value']['url'] ?? false;
        }
        if(($config['value']['caption'] ?? null) instanceof Closure) {
            $caption = call_user_func($config['value']['caption'], $record);
        }   else {
            if(!is_array($url)) {
                $caption = $url;
            }   else {
                $caption = Html::na();
            }
        }

        $caption = Html::a($caption, $url);
    }
}

?>

<?php ob_start() ?>
    <div class="view" data-rel="tooltip" data-placement="right" title="<?=strip_tags($caption)?>">
        <span>
            <?=$caption?>
        </span>
    </div>
<?=Html::tag('td', ob_get_clean(), \yii\helpers\ArrayHelper::merge($config['options'] ?? [], [
    'class' => 'column',
    'data-column' => $attribute
]))?>