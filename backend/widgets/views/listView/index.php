<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 24.03.2017
 * Time: 23:49
 * @var $provider \yii\data\ActiveDataProvider
 * @var $this \common\components\View
 * @var $config array
 * @var $instance \common\components\BaseActiveRecord|\backend\traits\SearchModelTrait
 * @var $actions array
 * @var $search \backend\models\forms\SearchForm
 */

$this->subTitle = $provider->totalCount;
$this->breadcrumbs = [
    [
        'caption' => Yii::t(null, 'breadcrumb'),
        'url' => $instance->getCrudLink('index')
    ]
];

$records = $provider->getModels();

$search->page = $provider->pagination->page + 1;

?>

<div class="container-fluid">
    <?php
    /** @var $form \common\components\ActiveForm */
    $form = \common\components\ActiveForm::begin([
        'id' => 'form-' . $instance->formName(),
        'action' => [ $instance::cnc() . '/index' ],
        'method' => 'get',
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]) ?>

    <?php foreach(['limit', 'sort', 'page'] as $input):?>
        <?=
        $form
            ->field($search, $input)
            ->label(false)
            ->hint(false)
            ->error(false)
            ->hiddenInput([
                'name' => $input
            ])
        ?>
    <?php endforeach?>

    <div id="listViewWrapper">
        <?=$this->render('controls', [
            'instance' => $instance,
            'search' => $search,
            'provider' => $provider
        ])?>

        <table class="table mt-2 table-striped <?php /*table-sm*/?> table-bordered" id="listView">
            <?=$this->render('thead', [
                'config' => $config['main']['list'] ?? [],
                'instance' => $instance,
                'provider' => $provider,
                'form' => $form
            ])?>
            <?=$this->render('tbody', [
                'config' => $config['main']['list'] ?? [],
                'records' => $records
            ])?>
        </table>

        <?=$this->render('pagination', [
            'search' => $search,
            'provider' => $provider,
            'instance' => $instance,
        ])?>
    </div>
    <?php $form->end() ?>
</div>