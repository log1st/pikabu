<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 31.03.2017
 * Time: 4:37
 * @var $config array
 * @var $records \common\components\BaseActiveRecord[]|\backend\traits\SearchModelTrait[]
 */
?>

<tbody>
<?php foreach($records as $record):?>
    <tr class="record" data-record="<?=$record->id?>">
        <?php foreach($config as $key => $item):
            $attribute = \backend\helpers\SearchModelView::getAttributeParam($item, $key);
            ?>
            <?=$this->render('td', [
                'config' => $item,
                'attribute' => $attribute,
                'record' => $record
            ])?>
        <?php endforeach?>
        <td class="actions">
            <div class="view">
                <?=$this->render('actions', [
                    'config' => \yii\helpers\ArrayHelper::merge($record::actions(), \backend\helpers\SearchModelView::actions()),
                    'record' => $record
                ])?>
            </div>
        </td>
    </tr>
<?php endforeach?>
</tbody>
