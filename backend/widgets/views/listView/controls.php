<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 31.03.2017
 * Time: 6:30
 * @var $search \backend\models\forms\SearchForm
 * @var $provider \yii\data\ActiveDataProvider
 * @var $instance \common\components\BaseActiveRecord|\backend\traits\SearchModelTrait
 */
use common\helpers\Html;

?>

<div class="d-flex align-items-center">
    <div class="summaries">
        <?=Yii::t(null, 'summaries', [
            'from' => $from = $search->page * $search->limit,
            'to' => $from + $provider->count,
            'total' => $provider->totalCount
        ])?>
    </div>
    <div class="limit-field">
        <div class="d-flex align-items-center f-12">
            <div class="pr-2">
                <?=Yii::t(null, 'limit')?>:
            </div>
            <div>
                <?php foreach($search::limits() as $limit):
                    $url = \yii\helpers\ArrayHelper::merge([ $instance::cnc() . '/index' ], Yii::$app->request->get(), [
                        'limit' => $limit
                    ]);
                    $current = (int)$search->limit === $limit;
                    ?>
                    <?=Html::a($limit, $url, \yii\helpers\ArrayHelper::merge(
                    [
                        'class' => 'btn btn-sm btn-' . ($current ? 'primary disabled' : 'link'),
                    ],
                    $current ? [
                        'disabled' => 'disabled'
                    ] : []
                )
                )?>
                <?php endforeach?>
            </div>
        </div>
    </div>
</div>
