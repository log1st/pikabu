<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 25.03.2017
 * Time: 0:44
 *
 * @var $instance \backend\traits\SearchModelTrait|\common\components\BaseActiveRecord
 * @var $config array
 * @var $this \common\components\View
 * @var $key string
 * @var $provider \yii\data\ActiveDataProvider
 * @var $form \common\components\ActiveForm
 */

use backend\widgets\AdminInput;
use common\helpers\Html;

$attribute = \backend\helpers\SearchModelView::getAttributeParam($config, $key);
$sort = $provider->sort->orders[$attribute] ?? null;

?>

<?php ob_start() ?>
    <div class="header">
        <?php if(isset($config['header'])):?>
            <?=$config['header']?>
        <?php else:?>
            <?=$instance->getAttributeLabel($attribute)?>
        <?php endif?>

        <?php if((bool)($config['sortable'] ?? true)):?>
            <?php ob_start() ?>
            <div>
                <i class="fa fa-sort-asc"></i>
            </div>
            <div>
                <i class="fa fa-sort-desc"></i>
            </div>

            <?=Html::a(ob_get_clean(), \yii\helpers\ArrayHelper::merge(
                [ Yii::$app->requestedRoute ],
                Yii::$app->request->get(),
                [
                    'sort' => ($sort === SORT_ASC ? '-' : '') . $attribute
                ]
            ), \yii\helpers\ArrayHelper::merge(
                [
                    'class' => 'sort',
                    'tabindex' => -1
                ],
                $sort ? [
                    'data-sort' => $sort === SORT_ASC ? 'asc' : 'desc'
                ] : [])
            )?>
        <?php endif?>
    </div>
    <div class="filter mt-2">
        <?php if(is_string($config) || !isset($config['filter'])):?>
            <?=AdminInput::widget([
                'field' => 'textInput',
                'attribute' => $attribute,
                'record' => $instance,
                'form' => $form
            ])?>
        <?php elseif($config instanceof Closure):?>
            <?=call_user_func($config, $instance)?>
        <?php elseif(is_array($config)):
            $white = [ 'field', 'attribute', 'record', 'form', 'options' ];
            $flip = array_flip($white);
            $normalized = array_intersect_key($config, $flip);
            ?>
            <?=AdminInput::widget(array_merge($normalized, [
                'form' => $form,
                'record' => $instance,
            ]))?>
        <?php endif?>
    </div>
<?=Html::tag('th', ob_get_clean(), \yii\helpers\ArrayHelper::merge($config['options'] ?? [], [
    'class' => 'column',
    'data-column' => $attribute
]))?>