<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 25.03.2017
 * Time: 0:44
 * @var $provider \yii\data\ActiveDataProvider
 * @var $instance \backend\traits\SearchModelTrait|\common\components\BaseActiveRecord
 * @var $config array
 * @var $this \common\components\View
 * @var $form \common\components\ActiveForm
 */
?>

<thead>
<tr>
    <?php foreach($config as $key => $item):?>
        <?=$this->render('th', [
            'instance' => $instance,
            'config' => $item,
            'key' => $key,
            'provider' => $provider,
            'form' => $form
        ])?>
    <?php endforeach?>
    <th class="actions">
        <div class="view">
            <?=Yii::t(null, 'actions.title')?>
        </div>
    </th>
</tr>
</thead>
