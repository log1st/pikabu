<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 31.03.2017
 * Time: 6:30
 * @var $search \backend\models\forms\SearchForm
 * @var $provider \yii\data\ActiveDataProvider
 * @var $instance \common\components\BaseActiveRecord|\backend\traits\SearchModelTrait
 */
use common\helpers\Html;

?>

<?php if(($count = $provider->pagination->pageCount) > 1): ?>
    <div class="text-center">
        <?php for($i = 1; $i <= $count; $i++):
            $url = \yii\helpers\ArrayHelper::merge([ $instance::cnc() . '/index' ], Yii::$app->request->get(), [
                'page' => $i
            ]);
            $current = (int)$search->page === $i;
            ?>
            <?=Html::a($i, $url, [
                'class' => 'btn btn-' . ($current ? 'primary disabled' : 'default')
            ])?>
        <?php endfor; ?>
    </div>
<?php endif?>