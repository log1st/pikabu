<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 31.03.2017
 * Time: 2:07
 * @var $record \common\components\BaseActiveRecord
 * @var $attribute string
 * @var $options array
 * @var $field string
 * @var $form \common\components\ActiveForm|null
 */

use common\helpers\Html;

if(!in_array($field, [
    'textInput'
])) {
    $field = 'textInput';
}

$name = Html::getInputName($record, $attribute);
$id = Html::getInputId($record, $attribute);

?>

<?php if($form instanceof \yii\widgets\ActiveForm && $record->hasAttribute($attribute)):?>
    <?php if($field === 'textInput'):?>
        <?=$form->field($record, $attribute, [
            'options' => [
                'class' => 'form-group mb-0'
            ]
        ])->label(false)->textInput($options)->render()?>
    <?php endif?>
<?php else: ?>
    <?php if($field === 'textInput'):?>
        <?=Html::textInput($name, $record->{$attribute} ?? null, array_merge([
            'class' => 'form-control'
        ], $options))?>
    <?php endif?>
<?php endif?>