<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 08.04.2017
 * Time: 2:02
 * @var $this \common\components\View
 * @var $scenario string
 * @var $record \common\components\BaseActiveRecord|\backend\traits\SearchModelTrait
 * @var $config array
 */
use common\helpers\Html;

$this->subTitle = '#' . $record->id;
$this->breadcrumbs = [
    [
        'caption' => Yii::t(null, 'breadcrumb.index'),
        'url' => $record->getCrudLink('index')
    ],
    [
        'caption' => $record,
        'url' => $record->getCrudLink('view')
    ]
];

$multi = count($config) > 1;

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block <?=$multi ? 'p-0': ''?>">
                    <?php if($multi):?>
                        <div class="row" data-rel="tabs">
                            <div class="col-2 pr-0">
                                <ul class="nav flex-column nav-pills b-r">
                                    <?php $i = 0; foreach($config as $key => $item):?>
                                        <li class="nav-item">
                                            <?=Html::a(Yii::t(null, 'nav.' . $record::className() . '.' . $key), '#' . $key, [
                                                'class' => 'nav-link rounded-0 ' . ($i === 0 ? 'active' : ''),
                                                'data-rel' => 'tabs-header'
                                            ])?>
                                        </li>
                                    <?php $i++; endforeach?>
                                </ul>
                            </div>
                            <div class="col-10 pl-0">
                                <div class="p-3">
                                    <?php $i = 0; foreach($config as $key => $group):?>
                                        <div <?=$i === 0 ? 'class="active"' : ''?> data-rel="tabs-pane" id="<?=$key?>">
                                            <?=$this->render('_group', [
                                                'key' => $key,
                                                'group' => $group,
                                                'record' => $record
                                            ])?>
                                        </div>
                                    <?php $i++; endforeach?>
                                </div>
                            </div>
                        </div>
                    <?php else:?>
                        <div data-rel="tabs-pane" id="main">
                            <?=$this->render('_group', [
                                'key' => 'main',
                                'group' => $config['main'],
                                'record' => $record
                            ])?>
                        </div>
                    <?php endif?>
                </div>
            </div>
        </div>
    </div>
</div>