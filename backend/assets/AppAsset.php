<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/static';
    public $baseUrl = '@web/static';
    public $css = [
        'css/index.scss'
    ];
    public $js = [
        'js/main.js'
    ];
    public $depends = [
        'common\assets\BootstrapAsset'
    ];
}
