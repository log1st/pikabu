<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:48
 */

namespace backend\traits;


use yii\helpers\ArrayHelper;

trait SearchModelTrait
{
    abstract static function searchConfig() : array;
    abstract static function cnc() : string;
    abstract static function config() : array;
    abstract static function actions() : array;

    public function getCrudLink($scenario = 'view', $params = []) {
        if(!in_array($scenario, [ 'view', 'update', 'create', 'index', 'delete' ])) {
            $scenario = 'view';
        }
        return ArrayHelper::merge(
            [ self::cnc() . '/' . $scenario ],
            in_array($scenario, [ 'view', 'update', 'delete' ]) ? [ 'id' => $this->id ] : [],
            $params
        );
    }
}