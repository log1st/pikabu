<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:26
 * @var $this \common\components\View
 * @var $breadcrumbs []
 */
use common\helpers\Html;

$breadcrumbs = \yii\helpers\ArrayHelper::merge([
    [
        'caption' => Html::icon('fa fa-home'),
        'url' => [ 'site/index' ],
        'title' => Yii::t(null, 'main.breadcrumb'),
    ]
], (array)$breadcrumbs);

?>

<?php if($count = count($breadcrumbs)):?>
    <div class="container-fluid">
        <ol class="breadcrumb">
            <?php foreach($breadcrumbs as $i => $breadcrumb):?>
                <li
                    class="breadcrumb-item <?=$last = ($i == ($count - 1)) ? 'active' : ''?>"
                    <?php if(isset($breadcrumb['title'])):?> data-rel="tooltip" title="<?=$breadcrumb['title']?>"<?php endif?>
                >
                    <?=$last ? $breadcrumb['caption'] ?? '#' : Html::a($breadcrumb['caption'] ?? '#', $breadcrumb['url'] ?? '#')?>
                </li>
            <?php endforeach?>
        </ol>
    </div>
<?php endif?>