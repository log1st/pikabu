<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:26
 * @var $this \common\components\View
 * @var $content string
 */
?>

<?php if(Yii::$app->request->isPjax):?>
    <?=$this->render('_title')?>
<?php endif?>

<?=$this->render('_breadcrumbs', [
    'breadcrumbs' => $this->breadcrumbs
])?>

<?php if($this->displayTitle):?>
    <div class="container-fluid">
        <h1>
        <span>
            <?=$this->getTitle()?>
        </span>
            <?php if($this->subTitle):?>
                <small class="text-muted"><?=$this->subTitle?></small>
            <?php endif?>
        </h1>
    </div>
<?php endif?>

<?=$content?>