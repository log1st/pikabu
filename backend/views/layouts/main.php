<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use backend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?=$this->render('_title')?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?=\backend\widgets\Navbar::widget()?>

<div id="pjaxContent">
    <?=$this->render('_pjax', [
        'content' => $content
    ])?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
