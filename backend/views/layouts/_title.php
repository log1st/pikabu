<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:27
 * @var $this \common\components\View
 */
use common\helpers\Html;

?>

<title>
    <?=Html::encode($this->getTitle())?>
</title>
