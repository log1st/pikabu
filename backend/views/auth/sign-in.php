    <?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 21:06
 * @var $model \backend\models\forms\SignInForm
 */
use common\components\ActiveForm;
use common\helpers\Html;


$this->breadcrumbs = [
    [
        'caption' => Yii::t(null, 'breadcrumb'),
        'url' => [ 'auth/sign-in' ]
    ]
]

?>

<div class="row">
    <div class="col-xs-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-center">
        <?php /** @var $form ActiveForm */
        $form = ActiveForm::begin([
            'id' => 'form_sign-in',
            'action' => ['auth/sign-in'],
            'options' => [
                'data-action' => 'submitSelf',
                'encrypt' => 'multipart/form-data'
            ]
        ]) ?>

        <legend class="text-left"><?=Yii::t(null, 'legend')?></legend>

        <?=$form->field($model, 'login', [
            'options' => [
                'class' => 'text-left form-group'
            ]
        ])->label($model->getAttributeLabel('login'))->textInput()?>

        <?=$form->field($model, 'password', [
            'options' => [
                'class' => 'text-left form-group'
            ]
        ])->label($model->getAttributeLabel('password'))->passwordInput()?>

        <?=$form->field($model, 'rememberMe')->label($model->getAttributeLabel('rememberMe'))->checkbox()?>

        <?=Html::submitButton(Yii::t(null, 'submitButton'), [
            'class' => 'btn btn-primary btn-lg'
        ])?>

        <?php $form->end(); ?>
    </div>
</div>
