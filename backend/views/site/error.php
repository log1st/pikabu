<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 21:01
 * @var $exception \yii\web\HttpException
 * @var $this \common\components\View
 */
use common\helpers\Html;

$this->breadcrumbs = [
    [
        'caption' => Yii::t(null, 'breadcrumb'),
        'url' => Yii::$app->requestedRoute
    ]
];

$this->displayTitle = false;

?>

<div class="text-center mt-5 pl-4">
    <h1 class="display-1 mt-5">
        <?=trim($exception->getName(), '.\s')?>
        <small class="text-danger">
            <?=Yii::t(null, 'title', [ 'code' => $exception->statusCode ])?>
        </small>
    </h1>
    <div class="mt-4">
        <p>
            <?=trim($exception->getMessage(), '.\s')?>
        </p>
        <p>
            <b><?=Yii::t(null, 'description2')?></b>
        </p>
        <?=Html::a(
            Html::icon('fa fa-home') . Html::span(Yii::t(null, 'backButton'), [
                'class' => 'ml-2'
            ]),
            Yii::$app->request->referrer ?? ['site/index'], [
                'class' => 'btn btn-large btn-primary'
            ]
        )?>
    </div>
</div>