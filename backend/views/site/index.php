<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 10:01
 * @var $this \yii\web\View
 */
use common\components\ActiveForm;
use common\helpers\Html;
use common\models\User;

$user = User::current();
?>

<div class="container-fluid">
    <?php if($user):?>
        Hello, <?=$user->email?><br>
        <?php $form = ActiveForm::begin([
            'action' => [ 'auth/sign-out' ],
            'method' => 'POST',
        ])?>
        <?=Html::submitButton('Sign Out')?>
        <?php $form->end() ?>
    <?php else:?>
        Hello, guest. Wanna <?=Html::a('sign in', [ 'auth/sign-in' ])?>?
    <?php endif?>
</div>