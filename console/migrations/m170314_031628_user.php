<?php

use yii\db\Migration;

class m170314_031628_user extends Migration
{

    public function safeUp()
    {
        $this->createTable('user', [
            'user_id' => $this
                ->primaryKey()
                ->notNull()
                ->comment('User ID'),
            'email' => $this
                ->string(320)
                ->notNull()
                ->comment('E-mail'),
            'password' => $this
                ->text()
                ->notNull()
                ->comment('Password hash'),
            'authKey' => $this
                ->text()
                ->notNull()
                ->comment('Auth key'),
            'accessToken' => $this
                ->text()
                ->notNull()
                ->comment('Access token'),
            'status' => $this
                ->integer(1)
                ->notNull()
                ->comment('Status')
        ]);

        return true;
    }

    public function safeDown()
    {
        $this->dropTable('user');

        return true;
    }
}
