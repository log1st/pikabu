<?php

use yii\db\Migration;

class m170313_190331_alterI18nTables extends Migration
{

    public function safeUp()
    {
        Yii::$app->runAction('migrate', [
            'migrationPath' => '@yii/rbac/migrations'
        ]);

        $this->dropForeignKey('fk_message_source_message', 'message');

        $this->renameColumn('source_message', 'id', 'source_id');
        $this->renameTable('source_message', 'i18n_source');

        $this->renameColumn('message', 'id', '_source_id');
        $this->dropPrimaryKey('language', 'message');
        $this->addColumn(
            'message',
            'message_id',
            $this
                ->primaryKey()
                ->notNull()
                ->first()
                ->comment('Message ID')
        );
        $this->renameTable('message', 'i18n_translation');

        $this->addForeignKey(
            'fk-i18n_translation-i18n_source',
            'i18n_translation',
            '_source_id',
            'i18n_source',
            'source_id',
            'CASCADE',
            'RESTRICT'
        );

        return true;
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-i18n_translation-i18n_source', 'i18n_translation');

        $this->dropColumn('i18n_translation', 'message_id');
        $this->renameColumn('i18n_translation', '_source_id', 'id');
        $this->addPrimaryKey('language', 'i18n_translation', [ 'id', 'language' ]);
        $this->renameTable('i18n_translation', 'message');

        $this->renameColumn('i18n_source', 'source_id', 'id');
        $this->renameTable('i18n_source', 'source_message');

        $this->addForeignKey(
            'fk_message_source_message',
            'message',
            'id',
            'source_message',
            'id',
            'CASCADE',
            'RESTRICT'
        );


        return true;
    }
}
