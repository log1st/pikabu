<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'sourceLanguage' => '00',
    'language' => 'en-US',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'loginUrl' => [ 'auth/sign-in' ]
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'converter' => [
                'forceConvert' => !YII_CACHE,
            ],
        ],
        'view' => [
            'class' => 'common\components\View'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array_merge(include_once('rules.php'), [

            ]),
        ],
        'i18n' => [
            'class' => 'common\components\I18N',
            'translations' => [
                '*' => [
                    'class' => \common\components\DbMessageSource::className(),
                    'cache' => YII_CACHE,
                    'sourceMessageTable' => '{{i18n_source}}',
                    'messageTable' => '{{i18n_translation}}',
                    'messageID' => '_source_id',
                    'forceTranslation' => true,
                    'on ' . \yii\i18n\MessageSource::EVENT_MISSING_TRANSLATION => [ \common\components\MissingTranslationEvent::className(), 'miss' ]
                ]
            ]
        ],
    ],
];
