<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

/**
 * @param $query \yii\db\ActiveQuery
 * @param bool $die
 */
function dq($query, $die = true) {
    echo $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
    if($die) {
        die;
    }
}