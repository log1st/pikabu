<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 5:34
 */

return [
    '/' => 'site/index',

    'login' => 'auth/sign-in',
    'join' => 'auth/sign-up',
    'logout' => 'auth/sign-out',
];