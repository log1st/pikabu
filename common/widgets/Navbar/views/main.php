<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 21:20
 * @var $menu array
 * @var $this \yii\web\View
 */
use common\helpers\Html;

/** @var \common\widgets\Navbar\Navbar $context */
$context = $this->context;

$navbarId = 'navbarCollapse';

?>

<nav class="navbar navbar-toggleable-md navbar-light fixed-top bg-faded">
    <?= Html::button(Html::span('', [
        'class' => 'navbar-toggler-icon',
    ]), [
        'class' => 'navbar-toggler navbar-toggler-right',
        'data-toggle' => 'collapse',
        'data-target' => '#' . $navbarId,
        'aria-controls' => 'navbarCollapser',
        'aria-expanded' => 'false',
        'aria-label' => Yii::t('widgets/Navbar', 'view.collapser.label')
    ]) ?>
    <?= Html::a(Yii::t('widgets/Navbar', 'view.title'), ['site/index'], [
        'class' => 'navbar-brand'
    ]) ?>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <?php foreach ($menu as $h => $group): ?>
            <ul class="navbar-nav <?= ($group['position'] ?? 'left') === 'right' ? 'ml-auto' : 'mr-auto' ?>"
                id="navbarCollapse-<?= $h ?>">
                <?php foreach ($group['list'] ?? [] as $key => $item):
                    $parent = isset($item['children']);
                    $caption = $context->getCaption($item, $key);
                    $url = $item['url'] ?? '#';
                    ?>
                    <li class="nav-item <?= $parent ? 'dropdown' : '' ?> <?= ($item['active'] ?? false) ? 'active' : '' ?>">
                        <?= Html::a($caption . ($key == 0 ? Html::srOnly() : ''), $url, \yii\helpers\ArrayHelper::merge([
                            'class' => 'nav-link ' . ($parent ? 'dropdown-toggle' : '')
                        ], $parent ? [
                            'id' => 'navbarCollapse-' . $key . '-toggle',
                            'data-toggle' => 'dropdown',
                            'aria-haspopup' => 'true',
                            'aria-expanded' => 'false'
                        ] : [])) ?>
                        <?php if ($parent): ?>
                            <div class="dropdown-menu" aria-labelledby="navbarCollapse-<?= $key ?>-toggle">
                                <?php foreach ($item['children'] as $subKey => $subItem):
                                    $caption = $context->getCaption(\yii\helpers\ArrayHelper::merge($subItem, ['add' => $item]), $subKey);
                                    $url = $subItem['url'] ?? '#';
                                    ?>
                                    <?= Html::a($caption, $url, [
                                    'class' => 'dropdown-item'
                                ]) ?>
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>
                    </li>
                <?php endforeach ?>
            </ul>
        <?php endforeach ?>
    </div>
</nav>
