<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 21:20
 */

namespace common\widgets\Navbar;


use Yii;
use yii\base\Widget;

class Navbar extends Widget
{

    public $menu = [];

    public function init() {

        parent::init();

        $this->menu = [
            'main' => [
                'position' => 'left',
                'list' => [
                    [
                        'attribute' => 'home',
                        'url' => [ 'site/index' ]
                    ]
                ]
            ],
        ];
    }

    public function run() {
        return $this->render('@common/widgets/Navbar/views/main', [
            'menu' => $this->menu
        ]);
    }

    public function getCaption($item, $key) {
        $add = $item['add']['attribute'] ?? null;

        if (isset($item['caption'])) {
            $caption = $item['caption'];
        } else {
            if (isset($item['attribute'])) {
                $caption = Yii::t('widgets/Navbar', 'view.menu.' . ($add ? "$add." : '') . $item['attribute'] . '.caption');
            } else {
                $caption = '#' . ($key + 1);
            }
        }

        return $caption;
    }
}