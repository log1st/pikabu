<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 7:56
 */

namespace common\components;


use common\helpers\Html;
use Yii;
use yii\helpers\ArrayHelper;

class ActiveField extends \yii\widgets\ActiveField
{
    public $errorOptions = [
        'class' => 'form-control-feedback'
    ];

    public $labelOptions = [
        'class' => 'form-control-label'
    ];

    /**
     * @inheritdoc
     */
    protected function addAriaAttributes(&$options)
    {
        if ($this->addAriaAttributes) {
            if (!isset($options['aria-required']) && $this->model->isAttributeRequired($this->attribute)) {
                $options['aria-required'] =  'true';
            }
            if (!isset($options['aria-invalid'])) {
                if ($this->model->hasErrors($this->attribute)) {
                    $options['aria-invalid'] = 'true';
                    $options['class'] .= ' form-control-danger';
                }   else {
                    $options['class'] = isset($options['class']) ? ($options['class'] . ' form-control-success') : 'form-control-success';
                }
            }
        }
    }

    /**
     * Renders the opening tag of the field container.
     * @return string the rendering result.
     */
    public function begin()
    {
        if ($this->form->enableClientScript) {
            $clientOptions = $this->getClientOptions();
            if (!empty($clientOptions)) {
                $this->form->attributes[] = $clientOptions;
            }
        }

        $inputID = $this->getInputId();
        $attribute = Html::getAttributeName($this->attribute);
        $options = $this->options;
        $class = isset($options['class']) ? [$options['class']] : [];
        $class[] = "field-$inputID";
        if ($this->model->isAttributeRequired($attribute)) {
            $class[] = $this->form->requiredCssClass;
        }
        if ($this->model->hasErrors($attribute)) {
            $class[] = $this->form->errorCssClass;
        }   else {
            /** @var Model $model */
            $model = $this->model;
            if(isset($model->_loaded) && in_array($attribute, $model->_loaded)) {
                $class[] = $this->form->successCssClass;
            }
        }
        $options['class'] = implode(' ', $class);
        $tag = ArrayHelper::remove($options, 'tag', 'div');

        return Html::beginTag($tag, $options);
    }
}