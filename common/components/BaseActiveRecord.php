<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 20:58
 */

namespace common\components;


use Yii;
use yii\db\ActiveRecord;

/**
 * Class BaseActiveRecord
 * @package common\components
 *
 * @property $id integer
 */
class BaseActiveRecord extends ActiveRecord
{
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    public static $idStr = 'id';

    public function __get($attr)
    {
        if($attr === 'id') {
            $attr = trim($this::$idStr, '[]');
        }
        return parent::__get($attr);
    }

    public function attributeLabels()
    {
        $labels = [];
        foreach(array_keys($this->attributes) as $key) {
            $labels[$key] = Yii::t(null, 'attributeLabel.' . static::className() . '.' . $key);
        }

        return $labels;
    }

    public static function findAllAsArray($id, $mask, $values, $condition = [], $asModels = false, $callback = null, $having = [])
    {
        $query = static::find()->andWhere($condition)->having($having)->groupBy([
            static::tableName() . '.' . static::$idStr
        ]);

        /** @var BaseActiveRecord[] $records */
        $records = $query->all();

        $response = [];
        foreach($records as $record) {
            $tmp = $record;
            if(!$asModels) {
                $tmp = $mask;
                foreach ($values as $value) {
                    $tmp = preg_replace('/:(' . $value . ')/', $record->getAttribute($value), $tmp);
                }
            }   else {
                if($callback instanceof \Closure) {
                    $tmp = call_user_func($callback, $tmp);
                }
            }
            $response[$record->getAttribute($id)] = $tmp;
        }
        return $response;
    }
}