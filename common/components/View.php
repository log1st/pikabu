<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 12:24
 */

namespace common\components;


use Yii;

class View extends \yii\web\View
{
    public $breadcrumbs = [];
    public $subStrings = [];
    public $subTitle = '';
    public $displayTitle = true;

    public function getTitle() {
        $ctrl = Yii::$app->controller;
        return $this->title ?? Yii::t(null, implode('.', [
            $ctrl->id,
            $ctrl->action->id,
            'title'
        ]));
    }
}