<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 8:09
 */

namespace common\components;


class Model extends \yii\base\Model
{
    public $_loaded = [];

    public function setAttributes($values, $safeOnly = true)
    {
        if (is_array($values)) {
            $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes());
            foreach ($values as $name => $value) {
                if(!in_array($name, $this->_loaded)) {
                    $this->_loaded[] = $name;
                }
                if (isset($attributes[$name])) {
                    $this->$name = $value;
                } elseif ($safeOnly) {
                    $this->onUnsafeAttribute($name, $value);
                }
            }
        }
    }

}