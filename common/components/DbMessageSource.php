<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 22:44
 */

namespace common\components;


use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class DbMessageSource extends \yii\i18n\DbMessageSource
{
    public $sourceMessageID = 'source_id';
    public $messageID = 'id';

    public $messages = [];

    protected function loadMessagesFromDb($category, $language)
    {
        $sID = $this->sourceMessageID;
        $mID = $this->messageID;
        $mainQuery = (new Query())->select(
            [
                'message' => 't1.message',
                'translation' => 't2.translation'
            ])
            ->from([
                't1' => $this->sourceMessageTable,
                't2' => $this->messageTable
            ])
            ->where([
                't1.' . $sID => new Expression('[[t2.' . $mID . ']]'),
                't1.category' => $category,
                't2.language' => $language
            ]);

        $fallbackLanguage = substr($language, 0, 2);
        $fallbackSourceLanguage = substr($this->sourceLanguage, 0, 2);

        if ($fallbackLanguage !== $language) {
            $mainQuery->union($this->createFallbackQuery($category, $language, $fallbackLanguage), true);
        } elseif ($language === $fallbackSourceLanguage) {
            $mainQuery->union($this->createFallbackQuery($category, $language, $fallbackSourceLanguage), true);
        }

        $messages = $mainQuery->createCommand($this->db);

        $messages = $messages->queryAll();


        $this->messages = $messages;
        $messages = ArrayHelper::map($messages, 'message', 'translation');
        return $messages;
    }

    protected function createFallbackQuery($category, $language, $fallbackLanguage)
    {
        $sID = $this->sourceMessageID;
        $mID = $this->messageID;

        return (new Query())->select(['message' => 't1.message', 'translation' => 't2.translation'])
            ->from(['t1' => $this->sourceMessageTable, 't2' => $this->messageTable])
            ->where([
                't1.' . $sID => new Expression('[[t2.' . $mID . ']]'),
                't1.category' => $category,
                't2.language' => $fallbackLanguage
            ])->andWhere([
                'NOT IN', 't2.' . $mID, (new Query())->select('[[' . $mID . ']]')->from($this->messageTable)->where(['language' => $language])
            ]);
    }
}