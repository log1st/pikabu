<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 14.03.2017
 * Time: 6:34
 */

namespace common\components;


class ActiveForm extends \yii\widgets\ActiveForm
{
    public $enableClientScript = false;

    public $errorCssClass = 'has-danger';

    public $successCssClass = 'has-success';

    public $fieldClass = 'common\components\ActiveField';
}