<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 22:01
 */

namespace common\components;


use Yii;
use yii\db\Query;
use yii\web\ServerErrorHttpException;

class MissingTranslationEvent extends \yii\i18n\MissingTranslationEvent
{
    /**
     * @param $event \yii\i18n\MissingTranslationEvent
     * @return string
     * @throws ServerErrorHttpException
     */
    public function miss($event) {
        $category = $event->category;
        $message = $event->message;

        /** @var DbMessageSource $sender */
        $sender = $event->sender;

        $query = (new Query())
            ->select($sender->sourceMessageID)
            ->from(
                [ 't1' => $sender->sourceMessageTable ]
            )
            ->where([
                '[[t1.category]]' => $category,
                '[[t1.message]]' => $message,
            ])
            ->one()
        ;

        if(!$query) {
            $command = Yii::$app->db->createCommand()->insert($sender->sourceMessageTable, [
                'category' => $category,
                'message' => $message
            ])->execute();

            if($command) {
                $id = Yii::$app->db->lastInsertID;
            }   else {
                throw new ServerErrorHttpException(Yii::t('db', 'unableToInsert'));
            }
        }   else {
            $id = $query[$sender->sourceMessageID];
        }

        return $message;
    }
}