<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 31.03.2017
 * Time: 7:25
 */

namespace common\components;


use backend\models\forms\SearchForm;

class Pagination extends \yii\data\Pagination
{

    /** @var SearchForm */
    public $search = null;

    /**
     * @inheritdoc
     */
    protected function getQueryParam($name, $defaultValue = null)
    {
        $params = $this->search->attributes;

        return isset($params[$name]) && is_scalar($params[$name]) ? $params[$name] : $defaultValue;
    }
}