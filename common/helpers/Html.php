<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 13.03.2017
 * Time: 21:25
 */

namespace common\helpers;


use Yii;

class Html extends \yii\helpers\Html
{
    public static function span($content = '', $options = []) {
        return parent::tag('span', $content, $options);
    }

    public static function srOnly($text = null) {
        if(is_null($text)) {
            $text = Yii::t(null, 'srOnly.label');
        }
        return Html::span($text, [
            'class' => 'sr-only'
        ]);
    }

    public static function icon($class) {
        return Html::tag('i', '', [
            'class' => $class
        ]);
    }

    public static function na($options = []) {
        return Html::tag('code', 'N/A', $options);
    }
}