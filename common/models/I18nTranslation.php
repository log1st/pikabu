<?php
/**
 * Created by PhpStorm.
 * I18nTranslation: log1s
 * Date: 12.02.2017
 * Time: 7:12
 */

namespace common\models;


use backend\models\I18nSourceSearch;
use common\components\BaseActiveRecord;

/**
 * Class I18nTranslation
 * @package console\models
 * @property integer $translation_id
 * @property string $_source_id
 * @property string $language
 * @property string $translation
 *
 * @property I18nSourceSearch $_source
 */
class I18nTranslation extends BaseActiveRecord
{
    public static $idStr = 'message_id';

    public function __toString()
    {
        return (string)$this->translation;
    }

    public static function tableName()
    {
        return '{{i18n_translation}}';
    }

    public function get_source() {
        return $this->hasOne(I18nSource::className(), [
            I18nSource::$idStr => '_' . I18nSource::$idStr
        ]);
    }
}