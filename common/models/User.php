<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 12.02.2017
 * Time: 7:12
 */

namespace common\models;


use common\components\BaseActiveRecord;
use Yii;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package console\models
 * @property integer $user_id
 * @property string $email
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property integer $status
 */
class User extends BaseActiveRecord implements IdentityInterface
{
    public static $idStr = 'user_id';

    public static $cnc = 'users';

    public function __toString()
    {
        return (string)$this->email;
    }

    public static function tableName()
    {
        return '{{user}}';
    }

    public static function findIdentity($id)
    {
        $identity = self::find()
            ->where([
                self::$idStr => $id,
                'status' => self::STATUS_ENABLED
            ])
            ->one();

        /** @var $identity IdentityInterface */
        return $identity;
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        $identity = self::find()
            ->where([
                'accessToken' => $token,
                'status' => self::STATUS_ENABLED
            ])
            ->one();

        /** @var $identity IdentityInterface */
        return $identity;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getAuthKey()
    {
        return $this->authKey;
    }


    public function validateAuthKey($authKey)
    {
        return (string)$this->authKey === (string)$authKey;
    }


    public static function findByIdentity($login) {
        return self::find()
            ->where([
                'email' => $login,
                'status' => self::STATUS_ENABLED
            ])
            ->one();
    }


    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return null|IdentityInterface|User
     */
    public static function current() {
        $user = Yii::$app->user;
        return $user->isGuest ? null : $user->identity;
    }

    /**
     * @return \yii\rbac\Role[]
     */
    public function getRoles() {
        $auth = Yii::$app->authManager;
        return $auth->getRolesByUser($this->id);
    }

    public function beforeSave($insert)
    {
        if(
            $insert || (mb_strlen($this->password) > 0)
        ) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }
        if(mb_strlen($this->password) === 0) {
            $this->password = $this->oldAttributes['password'];
        }
        return parent::beforeSave($insert);
    }
}