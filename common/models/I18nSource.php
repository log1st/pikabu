<?php
/**
 * Created by PhpStorm.
 * I18nSource: log1s
 * Date: 12.02.2017
 * Time: 7:12
 */

namespace common\models;


use common\components\BaseActiveRecord;

/**
 * Class I18nSource
 * @package console\models
 * @property integer $source_id
 * @property string $category
 * @property string $message
 */
class I18nSource extends BaseActiveRecord
{
    public static $idStr = 'source_id';

    public function __toString()
    {
        return (string)$this->message;
    }

    public static function tableName()
    {
        return '{{i18n_source}}';
    }
}